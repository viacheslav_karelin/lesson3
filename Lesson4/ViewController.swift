//
//  ViewController.swift
//  Lesson4
//
//  Created by Admin on 05.10.17.
//  Copyright © 2017 inix. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var inputTextField2: UITextField!
    @IBOutlet weak var startButton: UIButton!
    @IBAction func nextButton(_ sender: Any) {
        nextButtonPressed()
    }
    @IBAction func beforeButton(_ sender: Any) {
        beforeButtonPressed()
    }
    @IBAction func buttonStart(_ sender: UIButton) {
        startButtonPressed()
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        initJob(jobIndex: currentJob)
        
    }
    
    /*
     Описание домашнего задания:
     
     */
    
    var currentJob = 1      // Current Job
    var countOfAlljobs = 4  // Count of All jobs in hometask
    
    let jobDescription = [
        "taskDescription",
        "taskDescription",
        "taskDescription",
        "taskDescription",
    ]
    
    /*
     Inputs states:
     -1 - Disabled
     1 - first enabled
     2 - first and second enabled
     */
    let initJobInputFieldValue = [2, 1, 1, 1]
    
    /*
     
     */
    func task1() {

    }

    /*
     
     */
    func task2() {

    }
    
    /*
     
     */
    func task3() {

    }
    
    /*
     
     */
    func task4() {

    }
    
    func startButtonPressed(){
        print("Task: \(currentJob)")
        switch currentJob {
        case 1:
            task1()
        case 2:
            task2()
        case 3:
            task3()
        case 4:
            task4()
        default:
            return
        }
        initInputField(currentJob: currentJob)
    }
    
    func nextButtonPressed() {
        if currentJob + 1 <= countOfAlljobs {
            currentJob += 1
            initJob(jobIndex: currentJob)
        }
    }
    
    func beforeButtonPressed() {
        if currentJob - 1 > 0 {
            currentJob -= 1
            initJob(jobIndex: currentJob)
        }
    }
    
    func initJob(jobIndex: Int) {
        taskLabel.text = "Задание \(currentJob)"
        descriptionLabel.text = jobDescription[currentJob-1]
        setButtonTaskNumber(task: currentJob)
        initInputField(currentJob: currentJob)
    }
    
    func initInputField(currentJob: Int) {
        if initJobInputFieldValue[currentJob-1] == -1 {
            inputTextField.text = ""
            inputTextField.isEnabled = false
        }
        else
        {
            inputTextField.isEnabled = true
            inputTextField.text = ""
        }
    }
    
    // Show task number at button label
    func setButtonTaskNumber (task: Int) {
        let startLabel = "Start task # \(task)"
        startButton.setTitle(startLabel, for: .normal)
    }
    
    // Get integer number from input field, returning "-1" if it is not a number
    func getIntegerFromInputField(inputInstance: UITextField) -> Int {
        if let inputText = (inputInstance.text) {
            if let myNumber = Int(inputText) {
                // print("number is \(myNumber)")
                return myNumber
            }
            else {
                print ("Input field does not have number")
            }
        }
        inputTextField.text = ""
        return -1
    }
    
    // Get integer number from input field, returning "-1" if it is not a number
    func getDoubleFromInputField(inputInstance: UITextField) -> Double {
        if let inputText = (inputInstance.text) {
            if let myNumber = Double(inputText) {
                // print("number is \(myNumber)")
                return myNumber
            }
            else {
                print ("Input field does not have correct number")
            }
        }
        inputTextField.text = ""
        return -1
    }
}

